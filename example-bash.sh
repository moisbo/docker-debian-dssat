#!/usr/bin/env bash

for FOLDER in 1 .. 10
do
 docker run --rm -v "${USER}/experiments/${FOLDER}:/code" -w "/code" dssat-docker /bin/bash -c "/root/DSSAT47/dscsm047 A KSAS8101.WHX"
done
