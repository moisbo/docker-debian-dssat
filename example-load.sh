#! /bin/bash
# this will run a dssat-docker every X seconds
# run example-load.sh 10

max="$1"
date
echo "rate: $max runs / second"
START=$(date +%s);
FOLDER=0

loopit(){
 echo Folder: ${FOLDER}
 sudo cp -r /shared/loadinit /shared/loadtest/${FOLDER}
 docker run --rm -v "/shared/loadtest/${FOLDER}:/code" -w "/code" dssat-docker /bin/bash -c "/root/DSSAT47/dscsm047 A KSAS8101.WHX"
}

while true
do
  echo $(($(date +%s) - START)) | awk '{print int($1/60)":"int($1%60)}'
  sleep 1

  for i in `seq 1 $max`
  do
    FOLDER=$((FOLDER+1))
    loopit &
  done
done
