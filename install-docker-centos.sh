#! /bin/bash

#install docker and docker-compose centos

sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager \
  --add-repo \
  https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install -y docker-ce docker-ce-cli containerd.io

sudo systemctl start docker

sudo usermod -aG docker ${USER}

sudo yum install -y epel-release

sudo yum install -y python-pip

sudo pip install docker-compose

sudo yum upgrade -y python*

docker-compose version
