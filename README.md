# To get a docker image of dssat

clone this repository

git clone -b release https://github.com/DSSAT/dssat-csm.git

docker build -t dssat-csm .

# run detached

docker run --name dssat -dt dssat-csm:latest

# run with volume

docker exec -it dssat /bin/bash -v "/code:/Users/moises/Library/Mobile Documents/com~apple~CloudDocs/Trabajo/source/bitbucket/beckonapi.cropcloud.com.au/test-data/KSAS8101_WHX"

docker exec -it ad589b18ee65 /bin/bash -v "/code:/Users/moises/Library/Mobile Documents/com~apple~CloudDocs/Trabajo/source/bitbucket/beckonapi.cropcloud.com.au/test-data/KSAS8101_WHX"

## to access detached docker

docker exec -it dssat /bin/bash

## run     

Example:

dssat47/dscsm047 b dssbatch.v47 dscsm047.ctr


docker run --rm -v "/home/ec2-user/control_file:/code/run/" dssat-csm dssat b DSSBASTCH.v47 DSCM047.CTR


home/ec2-user/DSSAT47/experiments/5/KSAS8101.WHX

docker run --rm -v "/home/centos/experiments/1:/code/run" -w "/code/run" dssat-csm dssat c KSAS8101.WHX

docker run --rm -v --user 1000 "/home/ec2-user/experiments/1:/code/run" dssat-csm dssat c KSAS8101.WHX

docker run --rm -v "/home/ec2-user/moises:/code" -w "/code" dssat-docker /bin/bash -c "/root/DSSAT47/dscsm047 A KSAS8101.WHX"

docker run --rm -v "/home/ec2-user/experiments/1:/code" -w "/code" dssat-docker /bin/bash -c "/root/DSSAT47/dscsm047 A KSAS8101.WHX"
